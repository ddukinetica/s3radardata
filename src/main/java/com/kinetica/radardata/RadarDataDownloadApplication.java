package com.kinetica.radardata;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;


@SpringBootApplication
public class RadarDataDownloadApplication implements CommandLineRunner
{
	 private static final Logger LOGGER = LogManager.getLogger(RadarDataDownloadApplication.class.getName());
	 
	@Autowired
	private Environment env;
	
	
	
	public static void main(String[] args) 
	{
		
		  SpringApplication.run(RadarDataDownloadApplication.class, args);
		
	}
	@Override
    public void run(String...args) throws Exception {
		LOGGER.debug("Run args.length: " + args.length);
		for(String arg : args) {
			LOGGER.debug("Run arg: " + arg);
		}
		if(args.length <=5) {
			 LOGGER.debug("If using command line arguments, Usage: bucket_name date radar_sites dest_dir number_sites");
			 LOGGER.debug("Reading from properties file.");
		} 
		RadarDataDownloadApplication.dataDownload(env, args);
    }
	
	public static void dataDownload(Environment env, String...args) {
		
		 try {
			 Instant start = Instant.now();
			 
			 String bucketName = env.getProperty("bucket_name");
			 String date = env.getProperty("date");  // format: 2019/05/06
			 String radarSites = env.getProperty("radar_sites");  // KTLX
			 String destDir = env.getProperty("dest_dir");
			 String numberSites = env.getProperty("number_sites");
			 String accesskey = env.getProperty("accesskey");
			 String secretkey = env.getProperty("secretkey");
			 
			 if(accesskey.isEmpty() || secretkey.isEmpty()) {
				 LOGGER.error("Exception dataDownlod both access key and secretkey need to be configured. ");
				 System.exit(0);
			 }
			 LOGGER.debug("bucketName: "+bucketName);
			 LOGGER.debug("date: "+date);
			 LOGGER.debug("radarSites: "+radarSites);
			 LOGGER.debug("destDir: "+destDir); 
			 LOGGER.debug("numberSites: "+numberSites);
			 
			 
			 if(args.length>=4) {
				 bucketName = args[0];
				 date = args[1];
				 radarSites = args[2];
				 destDir = args[3];
				 numberSites = args[4];
			 }
			 if(numberSites.equalsIgnoreCase("ALL")){
				 
				 numberSites = "0";
			 }
			 int siteCount = Integer.valueOf(numberSites);  
			 
			 if(!destDir.endsWith(File.separator))
			      destDir = destDir+File.separator;
		     
		     //String bucket_name = "noaa-nexrad-level2";//"2019/05/06/KTLX/";
		     
			 LOGGER.debug("Objects in S3 bucketName: " + bucketName);
			 
			 AWSCredentials credentials = new BasicAWSCredentials(
					 accesskey, 
					 secretkey
					);
			 
			 
		     final AmazonS3 s3 = AmazonS3ClientBuilder.standard()  
		    		 .withRegion(Regions.US_EAST_1)
		    		 //.withCredentials(new MyCreds())
		    		 .withCredentials(new AWSStaticCredentialsProvider(credentials))
		    		 .build();
		     
		    // String prefix = date+"/"+radarSite+"/";//2019/05/06/KTLX/";
		     
		     
		     List<String> list = listKeysInBucket(bucketName, date, s3, radarSites);
		     long totalBytes = 0; int counterSite = 0; int counterFile = 0; 
		     int totalSites = list.size();
		     LOGGER.info("Total Number Of sites totalSites: " + totalSites);
		     for(String prefixKey : list) {
		    	    LOGGER.info(" loop each site prefixKey: " + prefixKey); 
		     
				    final ListObjectsV2Result result = s3.listObjectsV2(new ListObjectsV2Request()
				             .withPrefix(prefixKey)
				             .withBucketName(bucketName)
				             .withDelimiter("/")); 
				     
				     List<S3ObjectSummary> objects = result.getObjectSummaries();
				     
				     int size = objects.size();
				     LOGGER.info("Total Number Of Files: " + size +" in " + prefixKey);
				     counterFile = counterFile + size;
				     for (S3ObjectSummary os : objects) {
		
				         String key = os.getKey(); 
				         long bytes = os.getSize();
				         totalBytes = totalBytes + bytes;
				         LOGGER.info("Object key: " + key+" size: "+os.getSize());
				         
				         S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
				 
				         InputStream reader = new BufferedInputStream(object.getObjectContent());
				         int keyIndex = key.lastIndexOf("/");
				         if(keyIndex > 0) {
				        	 String filename = destDir+key.substring(keyIndex+1);
				        	 
			        		 File file = new File(filename);      
			        		 OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
			
			        		 int read = -1;
			
			        		 while ( ( read = reader.read() ) != -1 ) { 
			        		    writer.write(read);
			        		 } 
			        		 writer.flush();
			        		 writer.close();
			        		 reader.close(); 
				         } 
				        
				     }
				     if(counterSite >= siteCount && siteCount !=0) {
			        	 LOGGER.debug("Counter max reached.... " + counterSite);
			        	 break;
			         }
				     
			         LOGGER.debug(" Loop counter " + counterSite);
			         counterSite++;
			 }
		     Instant finish = Instant.now();
		     long timeElapsed = Duration.between(start, finish).toMillis();
		     LOGGER.debug(" Execution timeElapsed in milliseconds: " + timeElapsed);
		     LOGGER.debug(" Execution timeElapsed in seconds: " + timeElapsed/1000);
		     LOGGER.debug(" Execution total downloaded: " + totalBytes+ "bytes, which equals "+ totalBytes/1000000+"MB");
		     LOGGER.debug("Total Number Of sites totalSites: " + totalSites);
		     LOGGER.debug("Total Number Of Files counterFile: " + counterFile);
	     }catch(Exception e) {
	    	 //System.out.println("Exception dataDownload e: " + e.getMessage());
	    	 LOGGER.error("Exception dataDownload e: " + e.getMessage());
	    	 e.printStackTrace();
	     }
		 
	}
	public static List<String> listKeysInBucket(String bucketName, String prefix, AmazonS3 s3, String radarSites) {
		String originalPrefix = prefix;
	    Boolean isTopLevel = false;
	    String delimiter = "/";
	    if(prefix == "" || prefix == "/") {
	      isTopLevel = true;
	    }
	    if (!prefix.endsWith(delimiter)) {
	      prefix += delimiter;
	    }

	    ListObjectsRequest listObjectsRequest = null;
	    if (isTopLevel) {
	      listObjectsRequest =
	          new ListObjectsRequest().withBucketName(bucketName).withDelimiter(delimiter);
	    } else {
	      listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix)
	          .withDelimiter(delimiter);
	    }
	    ObjectListing objects = s3.listObjects(listObjectsRequest);
	    List<String> list = objects.getCommonPrefixes();
	    LOGGER.debug("listKeysInBucket list: " + list);
	    List<String> results = new ArrayList<String>();
	    String siteName = originalPrefix+"/"+radarSites;//.replaceAll("*", "");
	    LOGGER.debug("listKeysInBucket siteName: " + siteName);
	    for(String site: list) {
	    	if(site.toUpperCase().startsWith(siteName.toUpperCase())) {
	    		results.add(site);
	    	}
	    }
	    LOGGER.debug("listKeysInBucket results: " + results);
	    
	    return results;
	  }
	
}



class MyCreds extends DefaultAWSCredentialsProviderChain {
	private static final Logger LOGGER = LogManager.getLogger(DefaultAWSCredentialsProviderChain.class.getName());
	
	 @Override
	  public AWSCredentials getCredentials() {
	    try {
	      return super.getCredentials();
	    } catch (AmazonClientException ace) {

	    }

	    LOGGER.debug("No credentials available; falling back to anonymous access");
	    return new AnonymousAWSCredentials();
	  }
  }

